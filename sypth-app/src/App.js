import React, { Fragment } from 'react';
import { NavBar, UploadForm, FileList } from './components/index';
import { Provider } from 'react-redux';
import { GlobalStyle } from './utils/styles/global';
import store from './store';
import { BrowserRouter, Route } from 'react-router-dom';

const MainPage = () => (
  <Fragment>
    <GlobalStyle />
    <NavBar />
    <UploadForm />
    <FileList />
  </Fragment>
);

const App = () => (
  <Provider store={store}>
    <BrowserRouter>
      <Route path="/" component={MainPage} />
    </BrowserRouter>
  </Provider>
);
    
export default App;
import React, { Fragment } from 'react';
import Dropzone from 'react-dropzone';
import { Container, Form, Row, Col, Button, H1, H2, H3, H4, P } from './UploadForm.style';
import { FORM_VIEW, THANKYOU_VIEW, FORM_ERROR_VIEW } from '../../actions/FormViews';

const acceptedFilesItems = (acceptedFiles, hasFile, loading) => {
  if (!hasFile && acceptedFiles.length > 0) {
    return (
      <Button variant="secondary">or select file</Button>
    );
  }
  else {
    if (acceptedFiles.length > 0) {
      return (
        <Fragment>
          {loading ?
            <Fragment>
              <H4 className="selectedFile">{acceptedFiles[0].path}</H4>
            </Fragment>
            :
            <Fragment>
              <H4>{acceptedFiles[0].path}</H4>
            </Fragment>
          }
        </Fragment>
      )
    }
    else {
      return (
        <Button align="center" variant="secondary">or select file</Button>
      );
    }
  }
}

const UploadFormComponent = ({ hasToken, file, fileValid, formErrors, onSubmit, onDrop, newForm, view, loading }) => {
  if (view === FORM_VIEW && hasToken) {
    return (
      <Container>
        <Form onSubmit={onSubmit} encType="multipart/form-data">
          <Row>
            <Col sm={12}>
              <Dropzone
                multiple={false}
                onDropAccepted={onDrop}
              >
                {({ getRootProps, getInputProps, acceptedFiles }) => (
                  <div>
                    <Container className={fileValid ? "dragAndDrop" : "dragAndDrop error"} {...getRootProps()}>
                      <input {...getInputProps()} />
                      <H1>Submit your file</H1>
                      <H2 className="dranAndDrop">{loading ? `Loading...` : 'Drag & Drop'}</H2>
                      {loading ? null : <P className="toUpload">To upload</P>}
                      {acceptedFilesItems(acceptedFiles, file, loading)}
                    </Container>
                    <aside>
                      {fileValid ? null :
                        <H3 className="error">
                          {formErrors.file}
                        </H3>
                      }
                    </aside>
                  </div>
                )}
              </Dropzone>
            </Col>
          </Row>
          <Row>
            <Col sm={12} align="center">
              <Button variant="primary" type="submit" disabled={file ? false : true}>Submit</Button>
            </Col>
          </Row>
        </Form>
      </Container>
    );
  }
  else if (view === THANKYOU_VIEW) {
    return (
    <Container>
      <Row>
        <Col sm={12}>
          <H1 className="marginTop">Your file has been submitted!</H1>
        </Col>
      </Row>
      <Row>
        <Col sm={12} align="center">
          <Button className="newForm" variant="primary" onClick={newForm}>Upload new file</Button>
        </Col>
      </Row>
    </Container>
    );
  }
  else if (view === FORM_ERROR_VIEW) {
    return (
    <Container>
      <Row>
        <Col sm={12}>
          <H2 className="error">Error on upload your file.</H2>
        </Col>
      </Row>
      <Row>
        <Col sm={12} align="center">
          <Button className="newForm" variant="primary" onClick={newForm}>Try again</Button>
        </Col>
      </Row>
    </Container>
    );
  }
  else if (!hasToken) {
    return (
    <Container>
      <Row>
        <Col sm={12}>
          <H3 className="loading">Getting access permission...</H3>
          <H4>If this take too long consider refresh your browser or contact us.</H4>
        </Col>
      </Row>
    </Container>
    );
  }
  else {
    return null;
  }
}

export default UploadFormComponent;
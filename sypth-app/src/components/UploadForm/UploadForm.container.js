import React, { Component } from 'react';
import UploadFormComponent from './UploadForm.component';
import { connect } from 'react-redux';
import { getToken, newForm as newFormAction, submitForm as submitFormAction, changeForm as changeFormAction } from '../../reducers/UploadForm/UploadForm.selector';
import { bindActionCreators } from 'redux';

class UploadForm extends Component {
  constructor(props) {
    super(props);
    this.state = {
      formErrors: { file: ''},
      fileValid: true,
      formValid: true
    }
  }
  
  handleSubmit = e => {
    e.preventDefault();
    this.validateFields();
  }

  validateFields = () => {
    const { file } = this.props;
    let { formErrors, fileValid } = this.state;
    fileValid = !file ? false : true;
    formErrors.file = fileValid ? '' : 'Please update a file before submit';
    
    this.setState({
      formErrors,
      fileValid,
      formValid: fileValid
    }, this.submitAfterValidate);
  }

  submitAfterValidate = () => {
    if (this.state.formValid) {
      const { submitForm, file, token } = this.props;
      submitForm({token, file: file});
    }
  }

  handleOnDrop = acceptedFiles => {
    const { changeForm } = this.props;
    let file = acceptedFiles[0];
    changeForm("file", file);
  }

  handleNewForm = () => {
    const { newForm } = this.props;
    newForm();
  }

  render() {
    const { token, file, view, requestToken, loading } = this.props;
    const { fileValid, formErrors } = this.state;
    if (!token)
      requestToken();
  
    return(
      <UploadFormComponent
        hasToken={token ? true : false}
        file={file}
        fileValid={fileValid}
        formErrors={formErrors}
        onSubmit={this.handleSubmit}
        onDrop={this.handleOnDrop}
        newForm={this.handleNewForm}
        view={view}
        loading={loading}
      />
    )
  };
}

const mapStateToProps = state => ({
  token: state.form.token,
  file: state.form.file,
  loading: state.form.loading,
  view: state.form.view,
});

const mapDispatchToProps = dispatch => bindActionCreators({
  requestToken: getToken,
  newForm: newFormAction,
  submitForm: submitFormAction,
  changeForm: changeFormAction
}, dispatch)

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(UploadForm);
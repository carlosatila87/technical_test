import styled from 'styled-components';
import {Container as container, Form as form, Row as row, Col as col, Button as button} from 'react-bootstrap';

export const Container = styled(container)`
  &.dragAndDrop {
    border: dashed 2px #CCC;
    padding-top: 30px;
    padding-bottom: 30px;
    margin-bottom: 30px;
    text-align: center;
    width: 80%;
  }
`;
export const Form = styled(form)`
  margin-top: 30px;
`;
export const Row = styled(row)``;
export const Col = styled(col)``;
export const Button = styled(button)`
  &.newForm {
    margin-top: 50px;
  }
`;
export const H1 = styled.h1`
  font-size: 25px;
  text-align: center;
  &.marginTop {
    margin-top: 30px;
  }
`;
export const H2 = styled.h2`
  font-size: 18px;
  text-align: center;
  &.error {
    margin-top: 30px;
    color: #F60000;
  }
`;
export const H3 = styled.h3`
  font-size: 15px;
  text-align: center;
  &.loading {
    margin-top: 30px;
  }
`;
export const H4 = styled.h4`
  font-size: 12px;
  text-align: center;
`;
export const P = styled.p`
  text-align: center;
`;
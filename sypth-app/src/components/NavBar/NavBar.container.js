import React, {Component} from 'react';
import NavBarComponent from './NavBar.component';

class NavBar extends Component {
  render() {
    return (
      <NavBarComponent />
    )
  }
}

export default NavBar;
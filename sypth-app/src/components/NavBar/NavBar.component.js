import React from 'react';
import {Container, Row, Col, H1, H2, Ul} from './NavBar.style';

const NavBarComponent = () => (
  <Container>
    <Row>
      <Col md={3} sm={12}>
        <H1>Programming Exercise</H1>
        <H2>by Carlos Oliveira</H2>
      </Col>
      <Col md={9} sm={12} className="menuBar">
        <Ul>
          <li><a href="/" target="_self">Mandatory Problem</a></li>
        </Ul>
      </Col>
    </Row>
  </Container>
);

export default NavBarComponent;
import styled from 'styled-components';
import { Container as container, Row as row, Col as col } from 'react-bootstrap';

export const Container = styled(container)`
  border-bottom: 1px solid #CCC;
`;
export const Row = styled(row)``;
export const Col = styled(col)`
  text-align: left;
  &.menuBar {
    text-align: right;
  }
  @media (max-width: 767px) {
    text-align: center;
    &.menuBar {
      text-align: center;
    }
  }
`;
export const H1 = styled.h1`
  font-size: 25px;
`;
export const H2 = styled.h2`
  color: #CACACA;
  font-size: 18px;
`;
export const Ul = styled.ul`
  list-style-type: none;
  & li {
    display: inline-block;
    margin-top: 20px;
    margin-left: 40px;
  }
`;
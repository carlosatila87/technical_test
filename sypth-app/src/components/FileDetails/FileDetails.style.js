import styled from 'styled-components';
import {Table as table} from 'react-bootstrap';

export const Container = styled.div`
  height: 80vh;
  overflow: scroll;  
`;

export const H1 = styled.h1`
  text-align: center;
  font-size: 18px;
`;

export const H2 = styled.p`
  text-align: center;
  font-size: 18px;
`;

export const Table = styled(table)``;
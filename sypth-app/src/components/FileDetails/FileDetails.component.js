import React from 'react';
import { Container, H1, H2, Table } from './FileDetails.style';
import { Modal } from 'react-bootstrap';

const FileDetails = ({ show, closeModal, fileId, itemInfo }) => {
  return (
    <Modal
      show={show}
      size="xl"
      onHide={closeModal}
    >
      <Modal.Header closeButton>
      </Modal.Header>
      <Modal.Body>
        <Container>
          <H1>{fileId}</H1>
          {itemInfo.length > 0 ?
            (
              <Table>
                <thead>
                  <tr>
                    <th>Name</th>
                    <th>Value</th>
                    <th>Confidence</th>
                  </tr>
                </thead>
                <tbody>
                  {itemInfo.map(function (item, i) {
                    if (item.value) {
                      return (
                        <tr key={i}>
                          <td>{item.name}</td>
                          <td>{JSON.stringify(item.value)}</td>
                          <td>{item.confidence}</td>
                        </tr>
                      );
                    }
                    else{
                      return null;
                    }
                  })}
                </tbody>
              </Table>
            ) :
            (
              <H2>There is no specific info for this file.</H2>
            )
          }
        </Container>
      </Modal.Body >
    </Modal >
  );
};

export default FileDetails;
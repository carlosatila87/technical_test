import styled from 'styled-components';
import { Container as container, Row as row, Col as col, Table as table, Button as button } from 'react-bootstrap';

export const Container = styled(container)`
  margin-top: 50px;
`;
export const Row = styled(row)``;
export const Col = styled(col)``;
export const Table = styled(table)``;
export const Tr = styled.tr``;
export const Th = styled.th``;
export const Td = styled.td``;
export const Button = styled(button)``;
import React, { Component, Fragment } from 'react';
import FileListComponent from './FileList.component';
import FileDetails from '../FileDetails/FileDetails.component';
import { connect } from 'react-redux';

class FileList extends Component {
  constructor(props) {
    super(props);
    this.state = {
      showModal: false,
      fileId: null,
      itemInfo: []
    };
  }

  handleDetails = (fileId) => {
    const { token } = this.props;
    fetch(`${process.env.REACT_APP_RESULT_URL}${fileId}`, {
      headers: {
        'Authorization': `Bearer ${token}`
      }
    })
      .then(response => response.json())
      .then(response => {
        const { results } = response;
        const { fields } = results;
        this.setState({
          showModal: true,
          fileId: fileId,
          itemInfo: fields
        });
      });
  }

  closeModal = () => {
    this.setState({
      showModal: false,
      fileId: null,
      itemInfo: []
    });
  }

  render() {
    const { uploadedItems } = this.props;
    const { showModal, fileId, itemInfo } = this.state;
    return (
      <Fragment>
        <FileListComponent
          uploadedItems={uploadedItems}
          showDetails={this.handleDetails}
        />
        <FileDetails
          show={showModal}
          closeModal={this.closeModal}
          fileId={fileId}
          itemInfo={itemInfo}
        />
      </Fragment>
    )
  }
}

const mapStateToProps = state => ({
  token: state.form.token,
  uploadedItems: state.form.uploadedItems
});

export default connect(
  mapStateToProps
)(FileList);
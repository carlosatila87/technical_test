import React from 'react';
import { Container, Row, Col, Table, Tr, Th, Td, Button } from './FileList.style';

const FileListComponent = ({ uploadedItems, showDetails }) => {
  return (
    <Container>
      <Row>
        <Col>
          {uploadedItems.length > 0 ?
            (
              <Table>
                <thead>
                  <Tr>
                    <Th>File Id</Th>
                    <Th>Uploaded At</Th>
                    <Th>Status</Th>
                    <Th></Th>
                  </Tr>
                </thead>
                <tbody>
                  {uploadedItems.map(function (item, i) {
                    return (
                      <Tr key={i}>
                        <Td>{item.fileId}</Td>
                        <Td>{item.uploadedAt}</Td>
                        <Td>{item.status}</Td>
                        <Td><Button variant="outline-primary" onClick={() => showDetails(item.fileId)}>View</Button></Td>
                      </Tr>
                    );
                  })}
                </tbody>
              </Table>
            ) :
            null
          }
        </Col>
      </Row>
    </Container>
  );
}

export default FileListComponent;
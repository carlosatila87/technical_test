export const REQUEST_TOKEN = 'REQUEST_TOKEN';
export const TOKEN_RECEIVED = 'TOKEN_RECEIVED';
export const TOKEN_ERROR = 'TOKEN_ERROR';
export const SUBMIT_FORM = 'SUBMIT_FORM';
export const FORM_SUBMITTED = 'FORM_SUBMITTED';
export const FORM_SUBMIT_ERROR = 'FORM_SUBMIT_ERROR';
export const START_FORM = 'START_FORM';
export const CHANGE_FORM = 'CHANGE_FORM';

export const requestToken = () => {
  return {
    type: REQUEST_TOKEN
  }
}

export const tokenReceived = ({token}) => {
  return {
    type: TOKEN_RECEIVED,
    token
  }
}

export const tokenError = () => {
  return {
    type: TOKEN_ERROR
  }
}

export const submitForm = () => {
  return {
    type: SUBMIT_FORM
  }
}

export const formSubmitted = (data) => {
  return {
    type: FORM_SUBMITTED,
    data
  }
}

export const startForm = () => {
  return {
    type: START_FORM
  }
}

export const changeForm = (field, value) => {
  return {
    type: CHANGE_FORM,
    field,
    value
  }
}

export const formSubmitError = () => {
  return {
    type: FORM_SUBMIT_ERROR
  }
}
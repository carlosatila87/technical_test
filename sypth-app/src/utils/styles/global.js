import { createGlobalStyle } from 'styled-components';

export const GlobalStyle = createGlobalStyle`
  @import url('https://fonts.googleapis.com/css?family=Roboto&display=swap');
  body {
    font-family: 'Roboto', sans-serif;
    max-width: 100vw;
    height: 100vh;    
    margin: 0;
    padding: 0;
  }
`;
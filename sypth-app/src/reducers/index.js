import {combineReducers} from 'redux';
import UploadForm from './UploadForm/UploadForm.reducer';

export default combineReducers({
   form: UploadForm
});
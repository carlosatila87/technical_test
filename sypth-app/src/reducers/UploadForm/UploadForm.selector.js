import { requestToken, tokenReceived, startForm, formSubmitted, changeForm as changeFormAction, formSubmitError, submitForm as submitFormAction} from '../../actions/Actions';
import axios from 'axios';

export const getToken = () => {
  return dispatch => {
    dispatch(requestToken());
    axios.get('api/gettoken')
      .then(
        res => {
          dispatch(tokenReceived({ token: res.data.access_token }));
        }
      );
  }
}

export const newForm = () => {
  return dispatch => {
    dispatch(startForm());
  }
}

export const submitForm = ({ token, file }) => {
  return dispatch => {
    dispatch(submitFormAction());
    let config = {
      headers: {
        'Authorization': `Bearer ${token}`,
        'Content-Type': 'multipart/form-data'
      }
    };
    let formData = new FormData();
    formData.append('fileToUpload', file);
    axios.post(process.env.REACT_APP_UPLOAD_URL,formData,config)
      .then(
        res => {
          if (res.data.status === "RECEIVED")
            dispatch(formSubmitted(res.data));
        }
      ).catch(error => {
          dispatch(formSubmitError());
      }); 
  }
}

export const changeForm = (field, value) => {
  return dispatch => {
    dispatch(changeFormAction(field, value));
  }
}
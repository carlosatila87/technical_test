import { TOKEN_RECEIVED, CHANGE_FORM, SUBMIT_FORM, FORM_SUBMITTED, FORM_SUBMIT_ERROR, START_FORM } from '../../actions/Actions';
import {FORM_VIEW, THANKYOU_VIEW, FORM_ERROR_VIEW} from '../../actions/FormViews';

const initialState = {
  token: null,
  file: null,
  loading: false,
  view: FORM_VIEW,
  uploadedItems: [],
  loadingItems: true
};

export default function (state = initialState, action) {
  switch (action.type) {
    case TOKEN_RECEIVED: {
      const { token } = action;
      return {
        ...state,
        token
      }
    }
    case START_FORM: {
      const { token, uploadedItems } = state;
      return {
        ...initialState,
        token,
        uploadedItems
      }
    }
    case CHANGE_FORM: {
      const { field, value } = action;
      return {
        ...state,
        [field] : value
      }
    }
    case SUBMIT_FORM: {
       return {
         ...state,
         loading: true
       }
    }
    case FORM_SUBMITTED: {
      const { uploadedItems } = {...state};
      const {data} = action;
      const currentUploadedItems = [...uploadedItems, data];
      return {
        ...state,
        view: THANKYOU_VIEW,
        loading: false,
        uploadedItems: currentUploadedItems
      }
    }
    case FORM_SUBMIT_ERROR: {
      return {
        ...state,
        view: FORM_ERROR_VIEW,
        loading: false
      }
    }
    default:
      return state;
  }
}
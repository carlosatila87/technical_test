const express = require('express');
const bodyParser = require('body-parser');
const pino = require('express-pino-logger')();
const request = require('request');
const fs = require('fs');

const app = express();
app.use(bodyParser.urlencoded({ extended: false }));
app.use(pino);
app.use((req, res, next) => {
  res.header('Access-Control-Allow-Origin', '*');
  next();
});

app.get('/api/gettoken', (req, res) => {
  let options = {
    url: process.env.REACT_APP_TOKEN_URL,
    body: {
      client_id: process.env.REACT_APP_CLIENT_ID,
      client_secret: process.env.REACT_APP_CLIENT_SECRET,
      audience: process.env.REACT_APP_AUDIENCE,
      grant_type: process.env.REACT_APP_GRANT_TYPE
    },
    headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json'
    },
    json: true
  }
  request.post(
    options,
    (error, response, body) => {
      if (error || response.statusCode !== 200) {
        return res.status(500).json({ type: 'error', message: err.message });
      }
      res.json(body);
    }
  );
});

app.get('/api/getimage', (req, res) => {
  let options = {
    url: `https://api.sypht.com/result/image/${fileId}`,
    headers: {
      'Authorization': `Bearer ${token}`
    },
    encoding: null
  }
  request.get(options, (error, response, body) => {
    fs.writeFileSync(filePath, body);
  })
});

app.listen(3001, () =>
  console.log('Express server is running on localhost:3001')
);
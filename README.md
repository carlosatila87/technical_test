# technical_test
This is a Technical Test to Sypht.


To run this project properly follow these steps:

- create a .env file in the root folder (sypht-app) to place the variables below with the correct information:
----REACT_APP_CLIENT_ID={CLIENT ID GENERATED BY Sypht CONSOLE}
----REACT_APP_CLIENT_SECRET={CLIENT SECRET GENERATED BY Sypht CONSOLE}
----REACT_APP_TOKEN_URL=https://login.sypht.com/oauth/token
----REACT_APP_UPLOAD_URL=https://api.sypht.com/fileupload
----REACT_APP_AUDIENCE=https://api.sypht.com
----REACT_APP_GRANT_TYPE=client_credentials
----REACT_APP_RESULT_URL=https://api.sypht.com/result/final/

- execute a npm install inside sypht-app folder
- execute npm run dev 
   by executing this command both react-app and node server will run in paralel.
   
Thank you!